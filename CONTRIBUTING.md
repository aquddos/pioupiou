### Tests

- clone this repository
- `pip install -e ".[dev]"`

### Pre-commit

---

To enforce most required guidelines, please use the precommit mechanism.
This will perform some automatic checks before committing.

To use it, you need the `pre-commit` Python
you can then install hooks with:

```shell script
pre-commit install
pre-commit install --hook-type commit-msg
```

Then, you will be able to notice the automatic checks when running `git commit`.

To run pre-commit on all files:

```shell script
cz changelog --incremental
```

## Commit and Changelog

This software do use commitizen. So you should make valid commit message.
to do so, we recommand you to type :

```shell script
cz commit --dry-run > .current_commit_message
git commit -eF .current_commit_message
```

you can also use cz check : see https://commitizen-tools.github.io/commitizen/check/

## create new version

To update the changelog:

```shell script
cz changelog --incremental --unreleased-version {new_version}
```

You can then add some manual change to changelog
commit the changelog commit as a bump commit:

```shell script
git commit -m "docs(CHANGELOG): Update changelog for version {new_version}"
```

Tag the new_version:

```shell script
git tag {new_version}
```

push the tag to master:

```shell script
git push origin master {new_version}
```
