## 0.8.1 (2021-01-08)

- remove tests from pypi packages
- for dev: improve git config

## 0.8.0 (2020-10-17)

### Feat

- handle image variation filter + add extra iptc keyword filter using exiftool

### BREAKING CHANGE

- Refs:#1

## 0.7.0 (2020-10-17)

- better toolings (pre-commit, etc…)
- some internals changes

## 0.6 (2020-09-28)

- Add high level api with AvatarGenerator class
- remove python 3.5 support

## 0.5.2 (2020-04-01)

## 0.5.1 (2020-03-31)

- Breaking change ! Avatar generation use seed per layer now to be sure to not obtain same result from different layer.

## 0.5.0 (2020-03-31)

- add Chooser mecanism to choose mecanism for choosing layer from a hash method of hashlib

## 0.4.0 (2019-07-06)

- fix issue related to predictibility/reproductibility of result with FolderAvatarTheme.
  PiouPiou for the same entry given, should return exactly same result in layers choosen.
  Pillow configuration may cause some difference in
  image file, but this case hasn't been reproduced yet. This is tested an you
  can verify the behavior on your system by running TestFolderAvatarTheme test
  https://framagit.org/inkhey/pioupiou/commit/067af0a7800f51a1b3a4e172f9ab47e3737d473d
- AvatarTheme rely on specific Random() instead of random to avoid side-effect :
  https://framagit.org/inkhey/pioupiou/commit/8f9535324adf85bc62dc19cd5cd3ab8a3b063862
- minors changes around README.txt

## 0.3.1 (2019-07-06)

## 0.3.0 (2019-07-04)

- Initial Feature
- First Pypi release
